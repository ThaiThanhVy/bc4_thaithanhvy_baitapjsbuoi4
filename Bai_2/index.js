/**
 * Input
 * Viết chương trình “Chào hỏi” các thành viên trong gia đình
 * 
 * Todo
 * Tạo 5 option cho người dùng nhập vào 
 * Gán giá trị cho 5 option
 * 
 * Output
 * In ra màn hình câu chào khi người dùng nhập vào từng thành niên
 * 
 */


















function guiLoiChao() {
    var thanhVien = document.getElementById("txt-thanh-vien").value;


    var loiChao = '';

    if (thanhVien == "chonThanhVien") {
        loiChao = 'Xin Chào Người Lạ Ơi !';

    } else if (thanhVien == "bo") {
        loiChao = 'Xin Chào Bố !';

    } else if (thanhVien == "me") {
        loiChao = 'Xin Chào Mẹ !';

    } else if (thanhVien == "anhTrai") {
        loiChao = 'Xin Chào Anh Trai !';

    } else {
        loiChao = 'Xin Chào Em Gái !';
    }
    
    document.getElementById("ketQua").innerHTML = `${loiChao}`;
}