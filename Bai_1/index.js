/**
 * Input 
 * cho người dùng nhập vào 3 số nguyên bất kỳ
 * 
 * Todo
 * Tạo 3 ô input 
 * làm theo công thức
 * 
 * Output
 * In ra màn hình 3 số nguyên đó theo thứ tự tăng dần
 * 
 * 
 */




function sapXep() {
    var a = document.getElementById("txt-so-thu-1").value * 1;
    var b = document.getElementById("txt-so-thu-2").value * 1;
    var c = document.getElementById("txt-so-thu-3").value * 1;
    var ketQua = '';

    if (a > b && b > c) {
        ketQua = `${c} < ${b} < ${a}`;

    } else if (a > b && a > c) {
        ketQua = `${b} < ${c} < ${a}`;

    } else if (a > b && c > a) {
        ketQua = `${b} < ${a} < ${c}`;

    } else if (b > c && a > c) {
        ketQua = `${c} < ${a} < ${b}`;

    } else if (b > c && c > a) {
        ketQua = `${a} < ${c} < ${b}`;

    } else {
        ketQua = `${a} < ${b} < ${c}`;
    }

    document.getElementById("result").innerHTML = `${ketQua}`;
}