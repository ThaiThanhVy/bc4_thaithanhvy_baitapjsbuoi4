/**
 * Input
 * Viết chương trình cho người dùng nhập 3 cạnh của tam giác
 * 
 * Todo
 * Tạo 3 ô input
 * Làm theo công thức :
 * Đều: Nếu 3 cạnh bằng nhau. Vd:a = 3, b=3 c=3
 * Cân: Nếu 2 cạnh bằng nhau. Vd: a=2, b=2, c=1
 * Vuông: c^2 = a^2 + b^2. Vd: a=3, b=4, c=5
 * 
 * Output
 * In ra màn hình tam giác đó là tam giác gì
 * 
 */













function duDoan() {
    var canh1 = document.getElementById("txt-canh-1").value * 1;
    var canh2 = document.getElementById("txt-canh-2").value * 1;
    var canh3 = document.getElementById("txt-canh-3").value * 1;

    var ketQua = '';
    if (canh1 == canh2 && canh2 == canh3) {
        ketQua = 'Tam giac deu';

    } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
        ketQua = 'Tam giac can';

    } else if (canh3 * canh3 == (canh1 * canh1) + (canh2 * canh2)) {
        ketQua = 'Tam giac vuong';

    } else {
        ketQua = '1 Tam giac nao do';
    }



    document.getElementById("result").innerHTML = `${ketQua}`
}